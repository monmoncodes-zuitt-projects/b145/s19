/*//from activity

#1
// Do not modify!
let students = [];
let sectionedStudents = [];

function addStudent(name) {
    // Your code here!
    students.push(name);
    console.log(name + " has been added to the list of students!");
}

#2
// Do not modify!
let students = [];
let sectionedStudents = [];

// Do not modify!
function addStudent(name) {
    students.push(name);
    console.log(name + " has been added to the list of students!");
}

// Do not modify!
function countStudents() {
    console.log("This class has a total of " + students.length + " enrolled students.");
}

// Do not modify!
function printStudents() {
    students.sort();
    students.forEach(function(student) {
        console.log(student);
    });
}


function findStudent(keyword) {
    // Your code here!
    // Loops through all elements of the array to find the student name that matches the keyword provided
    let match = students.filter(function(student) {
        // If the student name includes the keyword provided
        return student.toLowerCase().includes(keyword.toLowerCase());
    });
    // match is an array...
    if (match.length == 1) {
        console.log(match[0] + " is enrolled."); // if 1 student was found, print the first element (at index 0) in a formatted string
    } else if (match.length > 1) {
        console.log("Multiple students match this keyword."); // more than 1 student was found
    } else {
        console.log("No students match this keyword."); // 0 students were found
    }
}

#3
// Do not modify!
let students = [];
let sectionedStudents = [];

// Do not modify!
function addStudent(name) {
    students.push(name);
    console.log(name + " has been added to the list of students!");
}

function countStudents() {
    // Your code here!
    console.log("This class has a total of " + students.length + " enrolled students.");
}

#4

// Do not modify!
let students = [];
let sectionedStudents = [];

// Do not modify!
function addStudent(name) {
    students.push(name);
    console.log(name + " has been added to the list of students!");
}

// Do not modify!
function countStudents() {
    console.log("This class has a total of " + students.length + " enrolled students.");
}

// Do not modify!
function printStudents() {
    students.sort();
    students.forEach(function(student) {
        console.log(student);
    });
}

// Do not modify!
function findStudent(keyword) {
    let match = students.filter(function(student) {
        return student.toLowerCase().includes(keyword.toLowerCase());
    });
    if (match.length == 1) {
        console.log(match[0] + " is enrolled.");
    } else if (match.length > 1) {
        console.log("Multiple students match this keyword.");
    } else {
        console.log("No students match this keyword.");
    }
}


function addSection(section) {
    // Your code here!
    sectionedStudents = students.map(function(student) {
        return student + " - section: " + section;
    });
}


#5
// Do not modify!
let students = [];
let sectionedStudents = [];

// Do not modify!
function addStudent(name) {
    students.push(name);
    console.log(name + " has been added to the list of students!");
}

// Do not modify!
function countStudents() {
    console.log("This class has a total of " + students.length + " enrolled students.");
}


function printStudents() {
    // Your code here!
    students.sort(); // sorts the array in alphabetical order
    students.forEach(function(student) { // sorts through each element
        console.log(student); // print the name of the students one by one
    });
}

#6

// Do not modify!
let students = [];
let sectionedStudents = [];

// Do not modify!
function addStudent(name) {
    students.push(name);
    console.log(name + " has been added to the list of students!");
}

// Do not modify!
function countStudents() {
    console.log("This class has a total of " + students.length + " enrolled students.");
}

// Do not modify!
function printStudents() {
    students.sort();
    students.forEach(function(student) {
        console.log(student);
    });
}

// Do not modify!
function findStudent(keyword) {
    let match = students.filter(function(student) {
        return student.toLowerCase().includes(keyword.toLowerCase());
    });
    if (match.length == 1) {
        console.log(match[0] + " is enrolled.");
    } else if (match.length > 1) {
        console.log("Multiple students match this keyword.");
    } else {
        console.log("No students match this keyword.");
    }
}

// Do not modify!
function addSection(section) {
    sectionedStudents = students.map(function(student) {
        return student + " - section: " + section;
    });
}


function removeStudent(name) {
    // Your code here!
    let removeIndex = students.indexOf(name); // Get the index of the student (will be -1 if it does not exist)
    if (removeIndex >= 0) { // if a student with the name `name` was found
        students.splice(removeIndex, 1); // remove it via splice()
    }
    
}



*/
// create a location/storage where the friends list will be stored

let friendsList = [];
console.log(friendsList)
let res = document.getElementById("responseMessage")
let length = friendsList.length;
// Mutator functions
   // create a function to add new friend

function addNewFriend() {
      //get the data from the user
      let friendName = document.getElementById("friendName").value;
   //validate the information by creating a structure that will make sure that the input is NOT empty
   if (friendName !== "") {
      //proceed with the function
      friendsList.push(friendName);
      length = friendsList.length
   } else {
      res.innerHTML = '<h5 class="mb-2 mt-5 text-danger">Invalid value</h5>';
   }
}
// Iteration functions

//create function to check how many friends

function viewListLength() {
   // we will assess the length of the array and inform the user how many elements are there in the array
   if (length === 0) {
      //inform the user
      res.innerHTML = "You currently have zero friends"
   } else if (length > 0) {
      res.innerHTML = "You currently have " + length + " friends"    
   }
}
